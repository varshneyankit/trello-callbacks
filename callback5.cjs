const getBoardInfoByID = require("./callback1.cjs");
const getListsByBoardID = require("./callback2.cjs");
const getCardsByListID = require("./callback3.cjs");

function getInformation(name, boards, lists, cards) {
  try {
    const boardId = boards.find((board) => board.name == name).id;

    getBoardInfoByID(boardId, boards, (err, data) => {
      if (err) {
        console.error(err);
      } else {
        console.log(data);

        getListsByBoardID(boardId, lists, (err, data) => {
          if (err) {
            console.error(err);
          } else {
            console.log(data);

            const listIds = data
              .filter((listData) => {
                if (listData.name == "Mind" || listData.name == "Space") {
                  return true;
                } else {
                  return false;
                }
              })
              .map((listData) => listData.id);

            listIds.forEach((listId) => {
              getCardsByListID(listId, cards, (err, data) => {
                if (err) {
                  console.error(err);
                } else {
                  console.log(data);
                }
              });
            });
          }
        });
      }
    });
  } catch (error) {
    console.error(error);
  }
}

module.exports = getInformation;
