const getBoardInfoByID = require("./callback1.cjs");
const getListsByBoardID = require("./callback2.cjs");
const getCardsByListID = require("./callback3.cjs");

function getInformation(name, boards, lists, cards) {
  try {
    const boardId = boards.find((board) => board.name == name).id;

    getBoardInfoByID(boardId, boards, (err, data) => {
      if (err) {
        console.error(err);
      } else {
        console.log(data);

        getListsByBoardID(boardId, lists, (err, data) => {
          if (err) {
            console.error(err);
          } else {
            console.log(data);

            const listId = data.find((listData) => listData.name == "Mind").id;

            getCardsByListID(listId, cards, (err, data) => {
              if (err) {
                console.error(err);
              } else {
                console.log(data);
              }
            });
          }
        });
      }
    });
  } catch (error) {
    console.error(error);
  }
}

module.exports = getInformation;
