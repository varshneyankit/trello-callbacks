function getCardsByListID(listId, cards, callback) {
  try {
    setTimeout(() => {
      const cardsList = cards[listId];
      callback(null, cardsList);
    }, 2000);
  } catch (error) {
    console.error(error);
  }
}

module.exports = getCardsByListID;
