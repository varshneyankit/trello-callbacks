function getListsByBoardID(boardId, lists, callback) {
  try {
    setTimeout(() => {
      const list = lists[boardId];
      callback(null, list);
    }, 2000);
  } catch (error) {
    console.error(error);
  }
}

module.exports = getListsByBoardID;
