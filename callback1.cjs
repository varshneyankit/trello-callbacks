function getBoardInfoByID(boardId, boards, callback) {
  try {
    setTimeout(() => {
      const boardInfo = boards.find((boardData) => boardData.id == boardId);
      callback(null, boardInfo);
    }, 2000);
  } catch (error) {
    console.error(error);
  }
}

module.exports = getBoardInfoByID;
